$(".main-line li").mouseenter(()=>{
    // $(".sub-box").css("visibility","visible");
    $(".sub-box").css("padding-top","20px");
    $(".sub-box").css("padding-bottom","20px");
    $(".sub-box").css("height","175px");
    $(".sub-box").css("border-bottom-width","1px");
})
$(".main-menu").mouseleave(()=>{
    $(".sub-box").css("height","0px");
    $(".sub-box").css("padding-top","0px");
    $(".sub-box").css("padding-bottom","0px");
    $(".sub-box").css("border-bottom-width","0px");
    // $(".sub-box").css("visibility","hidden");
    // $(".sub-box").css("display","none");
    // $(".sub-box").css("opacity",0);
})
$(".menu-icon-resp").click(()=>{
    $(".main-menu-resp").css("height","calc(100% - 50px)");
    $(".menu-icon-resp").css("display","none");
    $(".close-menu-icon-resp").css("display","block");
    $("body").css("overflow","hidden");
})
$(".close-menu-icon-resp").click(()=>{
    $(".main-menu-resp").css("height","0px");
    $(".menu-icon-resp").css("display","block");
    $(".close-menu-icon-resp").css("display","none");
    $("body").css("overflow","auto");
})
$(document).ready(function(){
    $('.responsive-brands-list').slick({
        dots: false,
        rtl: true,
        infinite: false,
        speed: 350,
        slidesToShow: 5,
        prevArrow: '<div class="product-list-arrow" style="right:0;top:0;"><i class="fas fa-chevron-right"></i></div>',
        nextArrow: '<div class="product-list-arrow" style="left:0;top:0;"><i class="fas fa-chevron-left"></i></div>',
        slidesToScroll: 5,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3,
              infinite: true,
              dots: false
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
        ]
      });
 

      $('.responsive-products-list').slick({
        dots: false,
        rtl: true,
        infinite: false,
        speed: 350,
        prevArrow: '<div class="product-list-arrow" style="right:0;top:0;"><i class="fas fa-chevron-right"></i></div>',
        nextArrow: '<div class="product-list-arrow" style="left:0;top:0;"><i class="fas fa-chevron-left"></i></div>',
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              infinite: true,
              dots: false
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
        ]
      });


      $('.main-slider-box').slick({
        dots: false,
        rtl: false,
        centerPadding: '0px',
        fade: true,
        cssEase: 'linear',
        infinite: true,
        speed: 350,
        prevArrow: '<i class="fas fa-chevron-circle-right slider-arrow" style="right:45px;"></i>',
        nextArrow: '<i class="fas fa-chevron-circle-left slider-arrow" style="left:45px;"></i>',
        slidesToShow: 1,
        slidesToScroll: 1,
      });

      $('.horiz-list-items').slick({
        // slidesToShow: 9,
        slidesToScroll: 1,
        rtl: true,
        prevArrow: '<i class="fas fa-chevron-circle-right slider-arrow" style="right:-12px;"></i>',
        nextArrow: '<i class="fas fa-chevron-circle-left slider-arrow" style="left:9px;"></i>',
        autoplay: false,
        infinite: false,
        variableWidth: true,
        // autoplaySpeed: 350,
      });
});



// range slider
(function () {

  var parent = document.querySelector(".range-slider");
  if (!parent) return;

  var
  rangeS = parent.querySelectorAll(".range-input-range"),
  numberS = parent.querySelectorAll(".range-input-number");

  rangeS.forEach(function (el) {
    el.oninput = function () {
      var slide1 = parseFloat(rangeS[0].value),
      slide2 = parseFloat(rangeS[1].value);

      if (slide1 > slide2) {
        [slide1, slide2] = [slide2, slide1];
        // var tmp = slide2;
        // slide2 = slide1;
        // slide1 = tmp;
      }

      numberS[0].value = slide1;
      numberS[1].value = slide2;
    };
  });

  numberS.forEach(function (el) {
    el.oninput = function () {
      var number1 = parseFloat(numberS[0].value),
      number2 = parseFloat(numberS[1].value);

      if (number1 > number2) {
        var tmp = number1;
        numberS[0].value = number2;
        numberS[1].value = tmp;
      }

      rangeS[0].value = number1;
      rangeS[1].value = number2;

    };
  });

})();